import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Req,
  Res,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AddUserDTO } from '../schema/AddUserDTO';
import { UserService } from '../services/user.service';
import { UserEntity } from '../db/entities/user.entity';
import { LoginUserDTO } from '../schema/LoginUserDTO';
import EAccess from '../enums/access.enum';
import RolesGuard from '../guards/roles.guard';
import AuthenticationGuard from '../guards/authentication.guard';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/all')
  @UseGuards(AuthenticationGuard, new RolesGuard([EAccess.ADMIN]))
  async getUsers(): Promise<UserEntity[]> {
    return await this.userService.getAllUsers();
  }
  @Post('/signup')
  async createUser(
    @Body(new ValidationPipe()) addUserDTO: AddUserDTO,
    @Req() req,
    @Res() res,
  ): Promise<any> {
    const msg = await this.userService.addUser(addUserDTO);
    if (msg.message === 'ERROR') {
      res.status(HttpStatus.BAD_REQUEST).json(msg);
    } else {
      res.status(HttpStatus.OK).json(msg);
    }
  }

  @Post('/login')
  async login(
    @Body(new ValidationPipe()) loginCredentials: LoginUserDTO,
    @Req() req,
    @Res() res,
  ): Promise<any> {
    const res1 = await this.userService.login(loginCredentials);
    res.status(HttpStatus.UNAUTHORIZED).json(res1);
  }

  /*
  Revoke JWT Token on Logout so that no attacker can use the token to perform operations
* */
  @Post('/logout')
  async logout(
    @Body('access_token') accessToken: string,
    @Req() req,
    @Res() res,
  ): Promise<any> {
    const msg = await this.userService.logout(accessToken);
    if (msg.code === HttpStatus.BAD_REQUEST) {
      res.status(HttpStatus.BAD_REQUEST).json(msg);
    } else {
      res.status(HttpStatus.OK).json(msg);
    }
  }
}
