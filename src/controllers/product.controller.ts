import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Req,
  Res,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AddProductDTO } from '../schema/AddProductDTO';
import { ProductService } from '../services/product.service';
import { ProductEntity } from '../db/entities/product.entity';
import EAccess from '../enums/access.enum';
import RolesGuard from '../guards/roles.guard';
import AuthenticationGuard from '../guards/authentication.guard';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}
  @Post('/add')
  @UseGuards(AuthenticationGuard, new RolesGuard([EAccess.ADMIN]))
  async createProduct(
    @Body(new ValidationPipe()) addProductDTO: AddProductDTO,
    @Req() req,
    @Res() res,
  ): Promise<any> {
    const msg = await this.productService.addProduct(addProductDTO);
    if (msg.message === 'ERROR') {
      res.status(HttpStatus.BAD_REQUEST).json(msg);
    } else {
      res.status(HttpStatus.OK).json(msg);
    }
  }

  @Get('/all')
  async getAllProducts(): Promise<ProductEntity[]> {
    return await this.productService.getAllProducts();
  }
}
