import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Req,
  Res,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { CartService } from '../services/cart.service';
import { AddProductToUserCartDTO } from '../schema/AddProductToUserCartDTO';
import AuthenticationGuard from '../guards/authentication.guard';
import AuthService from '../services/auth.service';

@Controller('cart')
export class CartController {
  constructor(
    private readonly cartService: CartService,
    private readonly authService: AuthService,
  ) {}

  @Post('/new')
  @UseGuards(AuthenticationGuard)
  async createUser(@Req() req, @Res() res): Promise<any> {
    const user = await this.authService.fetchUser(req.headers.jwttoken);
    const msg = await this.cartService.assignCartToUser(user.email);
    if (msg.message === 'ERROR') {
      res.status(HttpStatus.BAD_REQUEST).json(msg);
    } else {
      res.status(HttpStatus.OK).json(msg);
    }
  }

  @Get('/getUserCart')
  @UseGuards(AuthenticationGuard)
  async getUserCart(@Req() req, @Res() res): Promise<any> {
    const user = await this.authService.fetchUser(req.headers.jwttoken);
    const msg = await this.cartService.getUserCart(user.email);
    if (msg.message === 'ERROR') {
      res.status(HttpStatus.BAD_REQUEST).json(msg);
    } else {
      res.status(HttpStatus.OK).json(msg);
    }
  }

  @Post('/addToCart')
  @UseGuards(AuthenticationGuard)
  async addProductToUserCart(
    @Body(new ValidationPipe())
    addProductToUserCartDTO: AddProductToUserCartDTO,
    @Req() req,
    @Res() res,
  ): Promise<any> {
    const user = await this.authService.fetchUser(req.headers.jwttoken);
    const msg = await this.cartService.addProductToCart(
      user.email,
      addProductToUserCartDTO,
    );
    if (msg.message === 'ERROR') {
      res.status(HttpStatus.BAD_REQUEST).json(msg);
    } else {
      res.status(HttpStatus.OK).json(msg);
    }
  }
}
