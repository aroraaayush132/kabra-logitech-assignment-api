import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { UserController } from './controllers/user.controller';
import { ProductController } from './controllers/product.controller';
import { CartController } from './controllers/cart.controller';
import { UserService } from './services/user.service';
import { CartService } from './services/cart.service';
import { ProductService } from './services/product.service';
import AuthService from './services/auth.service';
import EAccess from './enums/access.enum';
import RolesGuard from './guards/roles.guard';

@Module({
  imports: [TypeOrmModule.forRoot(typeOrmConfig)],
  controllers: [
    AppController,
    UserController,
    ProductController,
    CartController,
  ],
  providers: [
    AppService,
    UserService,
    CartService,
    ProductService,
    AuthService,
    { useValue: EAccess, provide: RolesGuard },
  ],
})
export class AppModule {}
