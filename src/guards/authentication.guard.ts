import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import * as JWT from 'jsonwebtoken';
import { UserEntity } from '../db/entities/user.entity';
import { RevokedJwtTokensEntity } from '../db/entities/revoked-jwt-tokens.entity';

@Injectable()
class AuthenticationGuard implements CanActivate {
  private mySecret = 'topSecret';

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const jwtToken = await request.headers.jwttoken;
    if (
      (await RevokedJwtTokensEntity.createQueryBuilder('revoked')
        .where('revoked.token = :value', {
          value: jwtToken,
        })
        .getCount()) != 0
    ) {
      return false;
    }
    const user: UserEntity = await this.validateJWTToken(jwtToken);
    if (user) {
      request.user = user;
      request.jwtToken = jwtToken;
      return true;
    } else {
      return false;
    }
  }

  public async validateJWTToken(jwtToken: string): Promise<UserEntity> {
    try {
      const decoded: any = JWT.verify(jwtToken, this.mySecret);
      const { user_ } = decoded;
      const { email } = user_;
      const user = await UserEntity.getUserByEmail(email);
      if (!user) {
        throw new UnauthorizedException('Unauthorised');
      }
      return user;
    } catch (e) {
      throw new UnauthorizedException('Unauthorised');
    }
  }
}

export default AuthenticationGuard;
