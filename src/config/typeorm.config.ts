import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 32769,
  username: 'root',
  password: 'root',
  database: 'kabra-logitech',
  entities: [__dirname + '/../db/entities/**{.ts,.js}'],
  synchronize: true,
};
