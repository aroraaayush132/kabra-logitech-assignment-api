import { IsEmail, IsNotEmpty } from 'class-validator';

export class AddUserDTO {
  @IsNotEmpty()
  fname: string;

  @IsNotEmpty()
  lname: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;

  @IsNotEmpty()
  contact: string;

  houseNo: string;
  area: string;
  city: string;
  state: string;
  pincode: number;
  country: string;
}
