import { IsNotEmpty } from 'class-validator';

export class AddProductDTO {
  @IsNotEmpty()
  productName: string;

  @IsNotEmpty()
  productImage: string;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  unitPrice: string;
}
