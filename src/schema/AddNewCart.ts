import { IsEmail, IsNotEmpty } from 'class-validator';

export class AddNewCart {
  @IsNotEmpty()
  @IsEmail()
  email: string;
}
