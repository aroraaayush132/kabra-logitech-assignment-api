import { IsEmail, IsNotEmpty } from 'class-validator';

export class GetUserCart {
  @IsEmail()
  @IsNotEmpty()
  email: string;
}
