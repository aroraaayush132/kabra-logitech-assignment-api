import { IsNotEmpty } from 'class-validator';

export class AddProductToUserCartDTO {
  @IsNotEmpty()
  productId: number;
}
