import { HttpStatus, Injectable } from '@nestjs/common';
import { AddUserDTO } from '../schema/AddUserDTO';
import { UserEntity } from '../db/entities/user.entity';
import { LoginUserDTO } from '../schema/LoginUserDTO';
import EMessages from '../enums/EMessages';
import AuthService from './auth.service';
import * as bcryprt from 'bcryptjs';
import JWTManager from '../logout/JWTManager';

@Injectable()
export class UserService {
  constructor(private readonly authService: AuthService) {}
  async addUser(addUserDTO: AddUserDTO): Promise<any> {
    const {
      fname,
      lname,
      email,
      password,
      area,
      city,
      contact,
      country,
      houseNo,
      pincode,
      state,
    } = addUserDTO;
    const userEntity = await UserEntity.create();
    userEntity.fname = fname;
    userEntity.lname = lname;
    userEntity.email = email;
    userEntity.password = password;
    userEntity.area = area;
    userEntity.city = city;
    userEntity.contact = contact;
    userEntity.country = country;
    userEntity.houseNo = houseNo;
    userEntity.pincode = pincode;
    userEntity.state = state;

    const user = await UserEntity.getUserByEmail(email);
    const user1 = await UserEntity.getUserByContact(contact);

    if (user || user1) {
      return {
        message: 'ERROR',
        data: 'Email or Contact is already in use.',
      };
    }

    const data = await UserEntity.save(userEntity);
    if (data) {
      return {
        message: 'SUCCESS',
        data: `User Created Successfully with email: ${email}!!`,
      };
    }
    return {
      message: 'ERROR',
      data: 'Error while creating user.',
    };
  }

  async getAllUsers() {
    return await UserEntity.getAllUsers();
  }

  async login(loginUser: LoginUserDTO): Promise<any> {
    const { email, password } = loginUser;
    const user: UserEntity = await UserEntity.getUserByEmail(email);

    if (!user) {
      return {
        message: EMessages.UNAUTHORIZED_REQUEST,
        code: HttpStatus.UNAUTHORIZED,
      };
    }
    if (await bcryprt.compare(password, user.password)) {
      return await this.authService.generateJWTToken(user);
    } else {
      return {
        message: EMessages.UNAUTHORIZED_REQUEST,
        code: HttpStatus.UNAUTHORIZED,
      };
    }
  }

  async logout(accessToken: string) {
    return await JWTManager.revoke(accessToken);
  }
}
