import { Injectable } from '@nestjs/common';
import { AddProductDTO } from '../schema/AddProductDTO';
import { ProductEntity } from '../db/entities/product.entity';

@Injectable()
export class ProductService {
  async addProduct(addProductDTO: AddProductDTO): Promise<any> {
    const { productName, description, productImage, unitPrice } = addProductDTO;
    const productEntity = ProductEntity.create();
    productEntity.product_name = productName;
    productEntity.description = description;
    productEntity.product_image = productImage;
    productEntity.unit_price = unitPrice;

    const data = await ProductEntity.save(productEntity);
    if (data) {
      return {
        message: 'SUCCESS',
        data: 'Product Created Successfully!!',
      };
    }
    return {
      message: 'ERROR',
      data: 'Error while creating Product.',
    };
  }

  async getAllProducts() {
    return await ProductEntity.find();
  }
}
