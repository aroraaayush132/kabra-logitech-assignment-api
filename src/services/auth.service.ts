import { Injectable, UnauthorizedException } from '@nestjs/common';
import * as JWT from 'jsonwebtoken';
import { UserEntity } from '../db/entities/user.entity';

@Injectable()
export default class AuthService {
  private mySecret = 'topSecret';

  public async generateJWTToken(user: UserEntity) {
    const expiresIn = 60 * 60;
    const payload = {
      user_: user,
    };
    const token = JWT.sign(payload, this.mySecret, { expiresIn });

    return { expires_in: expiresIn, access_token: token };
  }

  public async logout() {
    return 'Logout successful';
  }

  public async fetchUser(jwtToken: string): Promise<UserEntity> {
    try {
      const decoded: any = JWT.verify(jwtToken, 'topSecret');
      const { user_ } = decoded;
      const { email } = user_;
      const user = await UserEntity.fetchUser(email);
      if (!user) {
        throw new UnauthorizedException('Unauthorised');
      }
      return user;
    } catch (e) {
      throw new UnauthorizedException('Unauthorised');
    }
  }
}
