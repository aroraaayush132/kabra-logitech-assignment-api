import { Injectable } from '@nestjs/common';
import { CartEntity } from '../db/entities/cart.entity';
import { UserEntity } from '../db/entities/user.entity';
import { CartProductEntity } from '../db/entities/cart-product.entity';
import { AddProductToUserCartDTO } from '../schema/AddProductToUserCartDTO';

@Injectable()
export class CartService {
  async assignCartToUser(email: string): Promise<any> {
    const user = await UserEntity.getUserByEmail(email);
    const userCart = await CartEntity.getCartOfUser(user.id);

    if (userCart) {
      return {
        message: 'ERROR',
        data: `Cart already exists for ${email}!! `,
      };
    }

    const cart = await CartEntity.create({
      userId: user.id,
    });
    await CartEntity.save(cart);
    return {
      message: 'SUCCESS',
      data: `Cart Created Successfully for ${email}!! `,
    };
  }

  async getUserCart(email: string): Promise<any> {
    return CartProductEntity.getAllCartProductsOfUser(email);
  }

  async addProductToCart(
    email: string,
    addProductToUserCartDTO: AddProductToUserCartDTO,
  ): Promise<any> {
    const { productId } = addProductToUserCartDTO;
    const user = await UserEntity.getUserByEmail(email);
    const userCart = await CartEntity.getCartOfUser(user.id);

    // Check if the product is already in user cart then add 1 to quantity
    const check = await CartProductEntity.createQueryBuilder('q')
      .where('q.cart_id =:value1 and q.product_id =:value2', {
        value1: userCart.id,
        value2: productId,
      })
      .getCount();
    // console.log(check);
    if (check > 0) {
      // Product is already there in cart for user
      const data = await CartProductEntity.createQueryBuilder('q')
        .where('q.cart_id =:value1 and q.product_id =:value2', {
          value1: userCart.id,
          value2: productId,
        })
        .getOne();
      const quantity = data.quantity;
      data.quantity = quantity + 1;
      return CartProductEntity.save(data);
    }

    // Product is not there in cart so simply add the product in cart
    const addProductToUserCart = await CartProductEntity.create();
    addProductToUserCart.cartId = userCart.id;
    addProductToUserCart.productId = productId;
    addProductToUserCart.quantity = 1;
    return CartProductEntity.save(addProductToUserCart);
  }
}
