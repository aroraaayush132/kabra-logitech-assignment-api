import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CartProductEntity } from './cart-product.entity';

@Entity()
export class ProductEntity extends BaseEntity{
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  product_name: string;

  @Column()
  product_image: string;

  @Column()
  description: string;

  @Column()
  unit_price: string;

  @OneToMany(
    () => CartProductEntity,
    (cartProductEntity) => cartProductEntity.cart,
  )
  cartConnection: Promise<CartProductEntity[]>;
}
