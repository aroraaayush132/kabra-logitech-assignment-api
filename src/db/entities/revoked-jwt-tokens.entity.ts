import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class RevokedJwtTokensEntity extends BaseEntity{
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  token: string;
}
