import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CartEntity } from './cart.entity';
import * as Bcrypt from 'bcryptjs';

@Entity()
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  fname: string;

  @Column({ nullable: false })
  lname: string;

  @Column({ nullable: false, unique: true })
  email: string;

  @Column({ nullable: false })
  password: string;

  @Column({ nullable: false, unique: true })
  contact: string;

  @Column()
  houseNo: string;

  @Column()
  area: string;

  @Column()
  city: string;

  @Column()
  state: string;

  @Column()
  pincode: number;

  @Column()
  country: string;

  @Column({ default: 0 })
  access: number;

  @OneToOne(() => CartEntity, (cartEntity) => cartEntity.user, { eager: true })
  cart: CartEntity;

  @BeforeInsert()
  public async beforeInsertHooks() {
    this.password = Bcrypt.hashSync(this.password, 10); // Hash password
  }

  public static async findById(id: number): Promise<UserEntity> {
    return await UserEntity.findOne({
      select: ['fname', 'lname', 'email'],
      where: { id },
    });
  }

  public static async getAllUsers(): Promise<UserEntity[]> {
    return await UserEntity.find({
      select: ['fname', 'lname', 'email'],
    });
  }

  public static async getUserByEmail(email: string): Promise<UserEntity> {
    return await UserEntity.findOne({
      where: { email },
    });
  }

  public static async getUserByContact(contact: string): Promise<UserEntity> {
    return await UserEntity.findOne({
      where: { contact },
    });
  }

  public static async fetchUser(email: string): Promise<UserEntity> {
    return await UserEntity.createQueryBuilder('q')
      .where('q.email = :email', { email: email })
      .select('q.fname', 'firstname')
      .addSelect('q.lname', 'lastname')
      .addSelect('q.email', 'email')
      .getRawOne();
  }

  public static async removeUser(email: string): Promise<UserEntity> {
    return await UserEntity.remove(
      await UserEntity.findOne({ where: { email } }),
    );
  }
}
