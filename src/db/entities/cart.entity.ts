import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { UserEntity } from './user.entity';
import { CartProductEntity } from './cart-product.entity';

@Entity()
export class CartEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'user_id' })
  userId: number;

  @OneToOne(() => UserEntity, (userEntity) => userEntity.cart, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id' })
  user: Promise<UserEntity>;

  @OneToMany(
    () => CartProductEntity,
    (cartProductEntity) => cartProductEntity.product,
    { eager: true },
  )
  productConnection: Promise<CartProductEntity[]>;

  static async getCartOfUser(id: number): Promise<CartEntity> {
    return await CartEntity.findOne({ where: { userId: id } });
  }
}
