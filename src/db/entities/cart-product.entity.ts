import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CartEntity } from './cart.entity';
import { ProductEntity } from './product.entity';
import { UserEntity } from './user.entity';

@Entity()
export class CartProductEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'cart_id' })
  cartId: number;

  @Column({ name: 'product_id' })
  productId: number;

  @Column()
  quantity: number;

  @ManyToOne(() => CartEntity, (cartEntity) => cartEntity.productConnection)
  @JoinColumn({ name: 'cart_id' })
  cart: Promise<CartEntity>;

  @ManyToOne(
    () => ProductEntity,
    (productEntity) => productEntity.cartConnection,
  )
  @JoinColumn({ name: 'product_id' })
  product: Promise<ProductEntity>;

  static async getAllCartProductsOfUser(
    email: string,
  ): Promise<CartProductEntity[]> {
    const user = await UserEntity.getUserByEmail(email);
    const cart = await CartEntity.getCartOfUser(user.id);
    return await CartProductEntity.find({
      where: { cartId: cart.id },
    });
  }
}
